package com.qunzq.common.dbUtil;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.ParameterizedType;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * https://blog.csdn.net/renyaoyao_1215/article/details/70616082 关于这个类的一些说明
 * Created by Qun on 2019-09-14.
 */
public abstract class MyBatisDao<T> extends SqlSessionDaoSupport {

    protected static final String CREATE = "create";        //添加
    protected static final String CREATES = "creates";        //批量添加
    protected static final String DELETE = "delete";        //删除
    protected static final String DELETES = "deletes";        //批量删除
    protected static final String UPDATE = "update";        //更新
    protected static final String UPDATES = "updates";        //批量更新
    protected static final String LOAD = "load";            //单个主键查询对象
    protected static final String LOADS = "loads";            //主键列表查询对象列表
    protected static final String LIST = "list";            //列表条件查询
    protected static final String COUNT = "count";            //计数
    protected static final String PAGING = "paging";        //分页查询

    /**
     * Namespace should be simple className
     */
    public final String nameSpace;

    public MyBatisDao() {
        if (getClass().getGenericSuperclass() instanceof ParameterizedType) {
            nameSpace = ((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
                    .getActualTypeArguments()[0]).getSimpleName();
        } else {
            //解决cglib实现aop时转换异常
            nameSpace = ((Class<T>) ((ParameterizedType) getClass().getSuperclass().getGenericSuperclass())
                    .getActualTypeArguments()[0]).getSimpleName();
        }
    }

    /*自 mybatis-spring-1.2.0 以来， SqlSessionDaoSupport 的 setSqlSessionTemplate 和 setSqlSessionFactory 两个方法上的 @Autowired 注解被删除，
    这就意味着继承于 SqlSessionDaoSupport 的 DAO 类，它们的对象不能被自动注入 SqlSessionFactory 或 SqlSessionTemplate 对象。
    如果在 Spring 的配置文件中一个一个地配置的话，显然太麻烦。比较好的解决办法是在我们的 DAO 类中覆盖这两个方法之一，
    并加上 @Autowired 注解。那么如果在每个 DAO 类中都这么做的话，显然很低效。更优雅的做法是，写一个继承于 SqlSessionDaoSupport 的 BaseDao，
    在 BaseDao 中完成这个工作，然后其他的 DAO 类再都从 BaseDao 继承*/
    @Override
    @Autowired
    public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
        super.setSqlSessionFactory(sqlSessionFactory);

    }

    /**
     * sql语句的id
     *
     * @param id sql id
     * @return "nameSpace.id"
     */
    protected String sqlId(String id) {
        return nameSpace + "." + id;
    }

    /**
     * 添加对象
     *
     * @param t 范型对象
     * @return 增加记录数
     */
    public Boolean create(T t) {
        return getSqlSession().insert(sqlId(CREATE), t) == 1;
    }

    /**
     * 批量添加对象
     *
     * @param ts 范型对象
     * @return 增加记录数
     */
    public Integer creates(List<T> ts) {
        return getSqlSession().insert(sqlId(CREATES), ts);
    }


    /**
     * 删除
     *
     * @param id 主键
     * @return 删除记录数
     */
    public Boolean delete(Long id) {
        return getSqlSession().delete(sqlId(DELETE), id) == 1;
    }

    /**
     * 批量删除
     *
     * @param ids 主键列表
     * @return 删除记录数
     */
    public Integer deletes(List<Long> ids) {
        return getSqlSession().delete(sqlId(DELETES), ids);
    }

    /**
     * 更新对象
     *
     * @param t 范型对象
     * @return 更新记录数
     */
    public Boolean update(T t) {
        return getSqlSession().update(sqlId(UPDATE), t) == 1;
    }

    /**
     * 批量更新对象
     *
     * @param ts 范型对象
     * @return 更新记录数
     */
    public Integer updates(List<T> ts) {
        return getSqlSession().update(sqlId(UPDATES), ts);
    }


    /**
     * 查询单个对象
     *
     * @param id 主键
     * @return 对象
     */
    public T load(String id) {
        return getSqlSession().selectOne(sqlId(LOAD), id);
    }

    /**
     * 查询单个对象
     *
     * @param id 主键
     * @return 对象
     */
    public T load(Integer id) {
        return load(Long.valueOf(id));
    }

    /**
     * 查询单个对象
     *
     * @param id 主键
     * @return 对象
     */
    public T load(Long id) {
        return getSqlSession().selectOne(sqlId(LOAD), id);
    }

    /**
     * 批量查询
     *
     * @param ids 主键列表
     * @return 查询的集合
     */
    public List<T> loads(List<Long> ids) {
        if (ids.isEmpty()) {
            return Collections.emptyList();
        }
        return getSqlSession().selectList(sqlId(LOADS), ids);
    }

    /**
     * 查询对象列表
     *
     * @param t 范型对象
     * @return 查询到的对象列表
     */
    public List<T> list(T t) {
        return getSqlSession().selectList(sqlId(LIST), t);
    }

    /**
     * 查询所有对象列表
     *
     * @return 所有对象列表
     */
    public List<T> listAll() {
        return list((T) null);
    }

    /**
     * 查询对象列表
     *
     * @param criteria Map查询条件
     * @return 查询到的对象列表
     */
    public List<T> list(Map<?, ?> criteria) {
        return getSqlSession().selectList(sqlId(LIST), criteria);
    }
}
