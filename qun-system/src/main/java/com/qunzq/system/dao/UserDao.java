package com.qunzq.system.dao;

import com.qunzq.common.dbUtil.MyBatisDao;
import com.qunzq.system.model.User;
import org.springframework.stereotype.Repository;

/**
 * Created by Qun on 2019-09-14.
 */
@Repository
public class UserDao extends MyBatisDao<User> {

}
