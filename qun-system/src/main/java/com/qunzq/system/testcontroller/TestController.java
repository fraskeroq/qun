package com.qunzq.system.testcontroller;

import com.qunzq.system.model.User;
import com.qunzq.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Qun on 2019-09-14.
 */
@Controller
@RequestMapping("/test")
public class TestController {

    @Autowired
    private UserService userService;


    @RequestMapping(value = "/test")
    @ResponseBody
    public String signCustSign(HttpServletRequest request, HttpServletResponse response) {
        User user = userService.load(1l);
        return user.toString();
    }
}
