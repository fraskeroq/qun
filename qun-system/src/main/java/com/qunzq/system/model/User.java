package com.qunzq.system.model;

import lombok.Data;

/**
 * Created by Qun on 2019-09-14.
 */
@Data
public class User {
    private long id;
    private String name;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
