package com.qunzq.system.impl;

import com.qunzq.system.dao.UserDao;
import com.qunzq.system.model.User;
import com.qunzq.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Qun on 2019-09-14.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;


    @Override
    public User load(long id) {
        return userDao.load(id);
    }
}
