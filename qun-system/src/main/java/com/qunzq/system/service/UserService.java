package com.qunzq.system.service;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.qunzq.system.model.User;

/**
 * Created by Qun on 2019-09-14.
 */
public interface UserService {

    public User load(long id);
}
